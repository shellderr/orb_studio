import Glview from './glview.js';
import {GUI} from "./lib/dat.gui.module.min.js";
import poly from './pgms/poly.js';

let animate = true;
const canvas = document.querySelector('canvas');
canvas.style.cursor = 'none'

const _gui = {
    fields:[
        {
            animate: animate,
            onChange: (v)=>{
                if(v) glview.start(); else glview.stop();
            }
        }
    ]
}

const glview = new Glview(canvas, poly, [800,600], 0, new GUI(), _gui);
if(animate) glview.start(); else glview.frame();
