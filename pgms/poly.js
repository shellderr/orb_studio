import {solids, polyhedra, models} from './model.js';
import {loadObj, getNormals} from './loader.js';
import * as mat4 from '../lib/glmat/mat4.js';
import * as THREE from '../lib/three-bvh.min.js';
const {ADDITION, SUBTRACTION, INTERSECTION, DIFFERENCE, Brush, Evaluator} = THREE;
var state = 0, al = 0, animate = false, loop = false, c1 = 0, c2 = 0;
const vals = [],valsg = [];

const m = {
    geom1: null,
    geom2: null,
    model: null,
    cmodel: null,
    brush1: null,
    brush2: null,
    evaluator: null,
    result: null,
    r1: null,
    r2: null,
    q1: null,
    q2: null,
    idx: 2,
    speed: 1,
};

const g = {
    xoffs: .5,
    p_fov: 1.222,
    p_near: .1, 
    p_far: 20,
    eye_x: 0, 
    eye_y: 0, 
    eye_z: -1.5, 
    target_x: 0, 
    target_y: 0, 
    target_z: 1,
    a_hsv: [0,1,1],
    b_hsv: [0,1,1],
    c_hsv: [0,1,1],
    rx: .01,
    ry: .01,
    rz: 0,
    rv: 1,
    orbit: 0,
    q: false
};

const vs =/*glsl*/`#version 300 es
    precision mediump float;
    in vec4 position;
    in vec4 normal;
    uniform mat4 pmat;
    uniform mat4 vmat;
    uniform mat4 rmat;
    uniform float zdist;
    uniform float zsep;
    out vec3 vnormal;
    out vec3 vdir;
    out vec3 vpos;

    struct Light {
        vec3 pos;
        vec3 dir;
        vec3 col;
        float pow;
        float spec;
        float diff;
        float dz;
        float am;
        float lev;
    };

    uniform Light lighta;
    uniform Light lightb;
    uniform Light lightc;
    out Light alight;
    out Light blight;
    out Light clight;

    void main() {
        vec4 pos = position*rmat;
        vpos = pos.xyz;
        vnormal = (normal*rmat).xyz;
        pos.z += zdist; 
        vdir = vec3(0,0,-1) - pos.xyz;
        gl_Position = pmat*vmat*pos;
        alight = lighta;
        blight = lightb;
        clight = lightc;
        blight.pos.z += zsep;
        alight.dir = alight.pos*vec3(-1,-1,1) - pos.xyz;
        blight.dir = blight.pos*vec3(-1,-1,1) - pos.xyz;
        clight.dir = clight.pos*vec3(-1,-1,1) - pos.xyz;
    }
`;

const fs = /*glsl*/`#version 300 es
    precision mediump float;
    in vec3 vnormal;
    in vec3 vdir;
    in vec3 vpos;
    in float uvz;
    out vec4 fragColor;
    uniform float nz;
    uniform float smoothn;

    struct Light {
        vec3 pos;
        vec3 dir;
        vec3 col;
        float pow;
        float spec;
        float diff;
        float dz;
        float am;
        float lev;
    };

    in Light alight;
    in Light blight;
    in Light clight;

    float lighting(vec3 vnorm, vec3 lpos, float p, float s, float d, float dz, float am){
        vec3 light = normalize(lpos);
        vec3 ray = reflect(-light, vnorm);
        float spec = pow(max(0., dot(light, ray)) ,p);
        float diff = max(0., dot(vec3(light.xy,light.z+dz), vnorm));
        return.3*am+d*diff+s*spec;
    }

    float fresnel(vec3 vnorm, vec3 lpos){
        return 0.;
    }

    void main(){
        vec3 n = normalize(cross(dFdx(vpos), dFdy(vpos)));
        n.z -= nz;
        vec3 norm = mix(normalize(n), normalize(vnormal), smoothn);
        float la = lighting(norm, alight.dir, alight.pow, alight.spec, alight.diff, alight.dz, alight.am);
        float lb = lighting(norm, blight.dir, blight.pow, blight.spec, blight.diff, blight.dz, blight.am);
        float lc = lighting(norm, clight.dir, clight.pow, clight.spec, clight.diff, clight.dz, clight.am);
        vec3 c = la*alight.col*alight.lev + lb*blight.col*blight.lev + lc*clight.col*clight.lev;
        fragColor = vec4(c, 1); 
    }

`;

function loadModel(pgm, g, init=false){
    g.geom1 = new THREE.IcosahedronGeometry(1, 1);
    g.geom2 = new THREE.IcosahedronGeometry(1, 1);
    if(g.idx > 0){
        let aa = [0, 3, 3, 3], ab = [0, 0, 1, 1], ac = [0, 0, .94, .8];
        let cm = ab[g.idx];
        g.model = loadObj(Object.values(polyhedra)[aa[g.idx]], .8);
        if(cm)g.cmodel = concavePoly(g.model, 10, ac[g.idx]);
        let model = cm ?g.cmodel: g.model;
        let mv = [], mn = [];
        for(let t of model.indices.v)
            for(let i of t) mv.push(...model.vertices.v[i]);
        for(let t of model.indices.v)
            for(let i of t) mn.push(...model.vertices.vn[i]);
        g.geom1.setAttribute('position', new THREE.BufferAttribute(new Float32Array(mv), 3)); 
        g.geom1.setAttribute('normal', new THREE.BufferAttribute(new Float32Array(mn), 3)); 
        g.geom2.setAttribute('position', new THREE.BufferAttribute(new Float32Array(mv), 3)); 
        g.geom2.setAttribute('normal', new THREE.BufferAttribute(new Float32Array(mn), 3)); 
    }
    g.brush1 = new Brush(g.geom1);
    g.brush2 = new Brush(g.geom2);
    evaluate(g);
    if(init){
        pgm.arrays.position.data.set(g.result.geometry.attributes.position.array);
        pgm.arrays.normal.data.set(g.result.geometry.attributes.normal.array);
        pgm.count = g.result.geometry.attributes.position.count;
    }
    else updateBuffers(pgm, g.result.geometry);
}

function updateBuffers(pgm, geometry){
    let g = geometry.attributes; 
    pgm.gl.bindBuffer(pgm.gl.ARRAY_BUFFER, pgm.arrays.position.buffer);
    pgm.gl.bufferSubData(pgm.gl.ARRAY_BUFFER, 0, g.position.array);
    pgm.gl.bindBuffer(pgm.gl.ARRAY_BUFFER, pgm.arrays.normal.buffer);
    pgm.gl.bufferSubData(pgm.gl.ARRAY_BUFFER, 0, g.normal.array);
    pgm.count = g.position.count;  
}

function evaluate(m){  
    m.brush1.position.x = -g.xoffs;
    m.brush2.position.x = g.xoffs;
    m.brush1.updateMatrixWorld();
    m.brush2.updateMatrixWorld();
    m.result = m.evaluator.evaluate(m.brush1, m.brush2, ADDITION);
}

function setup(pgm){
    mat4.perspective(pgm.uniforms.pmat, g.p_fov, pgm.res[0]/pgm.res[1], g.p_near, g.p_far);
    mat4.lookAt(pgm.uniforms.vmat,[g.eye_x,0,g.eye_z],[0,0,g.target_z], [0,1,0]);
    pgm.arrays.position.data = new Float32Array(18000);
    pgm.arrays.normal.data = new Float32Array(18000);
    m.evaluator = new Evaluator();
    loadModel(pgm, m, true);
    setupMem(vals, pgm.uniforms, 3);
    setupMem(valsg, g, 3);
    window.loadPreset = loadPreset;
    let v = g.xoffs;
    vals[state].lighta.pos[0] = v - Math.log(1+v*v)*.35;
    vals[state].lightb.pos[0] = -(v - Math.log(1+v*v)*.35);
    m.r1 = new THREE.Matrix4();
    m.r2 = new THREE.Matrix4();
    m.q1 = new THREE.Quaternion();
    m.q2 = new THREE.Quaternion();
}

function render(pgm){
    pgm.rtime += .01*g.rv;
    pgm.time += .01*m.speed;
    rotate(m, g, pgm.rtime)
    evaluate(m);
    updateBuffers(pgm, m.result.geometry);
    if(animate){ al = fract(pgm.time);}
    setVals(vals, pgm.uniforms, al);
    setVals(valsg, g, al);
    mat4.lookAt(pgm.uniforms.vmat,[g.eye_x,g.eye_y,g.eye_z],[g.target_x,g.target_y,g.target_z], [0,1,0]);
    let x = pgm.uniforms.mouse[0]-.5;
    let y = pgm.uniforms.mouse[1]-.5;
    mat4.perspective(pgm.uniforms.pmat, g.p_fov, pgm.res[0]/pgm.res[1], g.p_near, g.p_far);
    mat4.rotate(pgm.uniforms.rmat, pgm.uniforms.rmat, g.orbit*Math.min(.07*Math.hypot(y, x),.05), [-y,-x,0]);
}

function rotate(m, g, t){
    if(g.q){
        m.q1.setFromAxisAngle(new THREE.Vector3(-g.rx, -g.ry, -g.rz).normalize(), t);
        m.q2.setFromAxisAngle(new THREE.Vector3(g.rx, g.ry, g.rz).normalize(), t);
        m.brush1.rotation.setFromQuaternion(m.q1);
        m.brush2.rotation.setFromQuaternion(m.q2);
    }else{
        m.r1.makeRotationFromEuler(new THREE.Euler(-g.rx, -g.ry, -g.rz));
        m.r2.makeRotationFromEuler(new THREE.Euler(g.rx, g.ry, g.rz));
        m.brush1.position.x = 0;
        m.brush2.position.x = 0;
        m.brush1.applyMatrix4(m.r1);
        m.brush2.applyMatrix4(m.r2);
    }
}

function fract(v){
    if(!loop)v = Math.min(v, 1.999);
    return (v*.5 - Math.trunc(v*.5))*2;
}

function lerp(v0, v1, v2, t){
    if(t > 1){
        t -= Math.floor(t);
        return (1 - t) * v1 + t * v2;
    }   return (1 - t) * v0 + t * v1;
}

const pgm = {
    arrays: {
        position: {
            components: 3,
            data: []
        },
        normal: {
            components: 3,
            data: []
        }
    },
    time: 0,
    rtime: 0,
    uniforms: {
        pmat: [1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1],
        vmat: [1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1],
        rmat: [1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1],
        zdist: .5,
        zsep: 0,
        smoothn: 0,
        nz: 0,
        lighta: {
            pos: [.0, .0, -.8],
            dir: [0,0,-1],
            col: [1,0,0],
            pow: 30,
            spec: 1,
            diff: .7,
            dz: 0,
            am: 0,
            lev: .6
        },
        lightb: {
            pos: [.0, .0, -.8],
            dir: [0,0,-1],
            col: [1,0,0],
            pow: 30,
            spec: 1,
            diff: .7,
            dz: 0,
            am: 0,
            lev: .6
        },
        lightc: {
            pos: [.0, .0, -.8],
            dir: [0,0,-1],
            col: [1,0,0],
            pow: 30,
            spec: 1,
            diff: .7,
            dz: 0,
            am: 0,
            lev: 0
        },
    },
    vs: vs,
    fs: fs,
    clearcolor: [0,0,0,1],
    drawMode: 'TRIANGLES',
    setupcb: setup,
    rendercb: render

};

pgm.gui = {
    name: null,
    open: true,
    fields:[
    {
        name: 'lerp',
        open: true,
        updateFrame: true,
        fields: [
        {
            state: [state, 0, 2, 1],
            onChange: v =>{
                state = v;
                updateGui(pgm.gui.fields, vals[state]);
                updateGui(pgm.gui.fields, valsg[state]);
                pgm.gui.fields[0].fields[1].ref.setValue(v);
            }
        },
        {
            lerp: [al, 0, 2, .001],
            onChange: v =>{al = Math.min(1.9999,v);}
        },
        {
            animate: animate,
            onChange: v =>{animate = v;}
        },
        {
            loop: loop,
            onChange: v =>{loop = v;}
        },
        {
            trig: ()=>{pgm.time = 0;}
        },
        {
            speed_: [m.speed, 0, 1, .001],
            onChange: v =>{
                valsg[0].speed = valsg[1].speed = valsg[2].speed = m.speed = v;
            },
             _ref: { set: 'speed' }
        },
        {
            name: 'copy',
            open: false,
            fields: [
                {
                    from: [0, 0, 2, 1],
                    onChange: v =>{c1 = v;}
                },
                {
                    to: [0, 0, 2, 1],
                    onChange: v =>{c2 = v;}
                },
                {
                    copy: ()=>{copyMem(vals, valsg, c1, c2);}
                },
                {
                    presetData: ()=>{
                        alert(JSON.stringify({vals: vals, valsg: valsg}));
                    }
                },
                {
                    load: '',
                    onChange: v =>{
                        loadPreset(v.replace('loadPreset(\`','').replace('\`);',''));
                    }
                }
            ]
        }
        ]
    },
    {
        name: 'poly',
        open: true,
        updateFrame: true,
        fields: [
            {
                poly_: [m.idx, 0, 3, 1],
                onChange: v =>{
                    if(v !== m.idx){
                        valsg[0].idx = valsg[1].idx = valsg[2].idx = m.idx = v;
                        loadModel(pgm, m);
                    }
                },
                _ref: { set: 'idx' }
            },
            {
                z: [pgm.uniforms.zdist, -2, 10, .01],
                onChange: v => {vals[state].zdist = v;},
                _ref: { set: 'zdist' }
            },
            {
                fov_: [g.p_fov, Math.PI/4, Math.PI, .01],
                onChange: v => {valsg[state].p_fov = v;},
                _ref: { set: 'p_fov'}
            },
            {
                normz: [pgm.uniforms.nz, -2, 2, .01],
                onChange: v => {vals[state].nz = v;},
                _ref: { set: 'nz'}
            },
            {
                norms: [pgm.uniforms.smoothn, 0, 1, .01],
                onChange: (v)=>{vals[state].smoothn = v;},
                _ref: { set: 'smoothn'}
            },
            {
                xoffs: [g.xoffs, 0, 3, .001],
                onChange: (v)=>{
                    valsg[state].xoffs = v;
                    vals[state].lighta.pos[0] = v - Math.log(1+v*v)*.35;
                    vals[state].lightb.pos[0] = -(v - Math.log(1+v*v)*.35);
                },
                _ref: { set: 'xoffs' }
            },
        ]
        },
        {
            name: 'rot',
            open: false,
            updateFrame: true,
            fields: [
                {
                    linear_: g.q,
                    onChange: (v)=>{
                        valsg[0].q = valsg[1].q = valsg[2].q = v;
                    },
                    _ref: { set: 'q' }
                },
                {
                    lin_rate: [g.rv, 0, 7, .001],
                    onChange: (v)=>{valsg[state].rv = v;},
                    _ref: { set: 'rv'}
                },
                {
                    rx: [g.rx, 0, .05, .001],
                    onChange: (v)=>{valsg[state].rx = v;},
                     _ref: { set: 'rx'}
                },
                {
                    ry: [g.ry, 0, .05, .001],
                    onChange: (v)=>{valsg[state].ry = v;},
                     _ref: { set: 'ry'}
                },
                {
                    rz: [g.rz, 0, .05, .001],
                    onChange: (v)=>{valsg[state].rz = v;},
                     _ref: { set: 'rz'}
                },
                {
                    orbit: [g.orbit, 0, 1, .001],
                    onChange: (v)=>{valsg[state].orbit = v;},
                     _ref: { set: 'orbit'}
                },
                {
                    resetOrbit: ()=>{
                        pgm.uniforms.rmat = [1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1];
                    }
                }
            ]
        },
        {
            name: 'view',
            open: true,
            updateFrame: true,
            fields: [
                {   
                    eyex: [g.eye_x, -2, 2, .001],
                    onChange: v =>{valsg[state].eye_x = v;},
                    _ref: { set: 'eye_x' }
                },
                {   
                    eyey: [g.eye_y, -2, 2, .001],
                    onChange: v =>{valsg[state].eye_y = v;},
                     _ref: { set: 'eye_y' }
                },
                {
                    eyez: [g.eye_z, -10, 5, .001],
                    onChange: v =>{valsg[state].eye_z = v;},
                     _ref: { set: 'eye_z' }
                },
                {
                    targetx: [g.target_x, -2, 2, .001],
                    onChange: v => {valsg[state].target_x = v;},
                     _ref: { set: 'target_x' }
                },
                {
                    targety: [g.target_y, -2, 2, .001],
                    onChange: v => {valsg[state].target_y = v;},
                     _ref: { set: 'target_y' }
                }
            ]
        },
        {
            name: 'tracking_light',
            open: true,
            updateFrame: true,
            fields: [
                {
                    z: [pgm.uniforms.lighta.pos[2], -2, 2, .01],
                    onChange: v => {
                        vals[state].lighta.pos[2] = v;
                        vals[state].lightb.pos[2] = v;
                    },
                    _ref: { set: 'lighta', sub: 'pos', idx: 2 }
                },
                {
                    zsep: [pgm.uniforms.zsep, -1, 1, .001],
                    onChange: v => {
                        vals[state].zsep = v;
                    }
                },
                {
                    pow: [pgm.uniforms.lighta.pow, 1, 40, .01],
                    onChange: v => {
                        vals[state].lighta.pow = v;
                        vals[state].lightb.pow = v;
                    },
                    _ref: { set: 'lighta', sub: 'pow' }
                },
                {
                    spec: [pgm.uniforms.lighta.spec, 0, 1, .01],
                    onChange: v => {
                        vals[state].lighta.spec = v;
                        vals[state].lightb.spec = v;
                    },
                     _ref: { set: 'lighta', sub: 'spec' }
                },
                {
                    diff: [pgm.uniforms.lighta.diff, 0, 1, .01],
                    onChange: v => {
                        vals[state].lighta.diff = v;
                        vals[state].lightb.diff = v;
                    },
                     _ref: { set: 'lighta', sub: 'diff'}
                },
                {
                    dz: [-pgm.uniforms.lighta.dz, 0, 2, .001],
                    onChange: v =>{
                        vals[state].lighta.dz = -v;
                        vals[state].lightb.dz = -v;
                    },
                     _ref: { set: 'lighta', sub: 'dz', inv: true}
                },
                {
                    am: [pgm.uniforms.lighta.am, 0, 1, .001],
                    onChange: v =>{
                        vals[state].lighta.am = v;
                        vals[state].lightb.am = v;
                    },
                     _ref: { set: 'lighta', sub: 'am'}
                },
                {
                    alev: [pgm.uniforms.lighta.lev, 0, 1.5, .01],
                    onChange: (v)=>{ vals[state].lighta.lev = v;},
                     _ref: { set: 'lighta', sub: 'lev' }
                },
                {
                    blev: [pgm.uniforms.lightb.lev, 0, 1.5, .01],
                    onChange: (v)=>{ vals[state].lightb.lev = v;},
                     _ref: { set: 'lightb', sub: 'lev' }
                },
                {
                    ahue: [0, 0, 1, .001],
                    onChange: (v)=>{
                        valsg[state].a_hsv[0] = v;
                        vals[state].lighta.col = hsv2rgb(...valsg[state].a_hsv);
                    },
                    _ref: {cb: (ref)=>{ ref.setValue(valsg[state].a_hsv[0])}}
                },
                {
                    bhue: [0, 0, 1, .001],
                    onChange: (v)=>{
                        valsg[state].b_hsv[0] = v;
                        vals[state].lightb.col = hsv2rgb(...valsg[state].b_hsv);
                    },
                    _ref: {cb: (ref)=>{ ref.setValue(valsg[state].b_hsv[0])}}
                },
            ]
        },
        {
            name: 'static_light',
            open: true,
            updateFrame: true,
            fields: [
                {
                    x: [pgm.uniforms.lightc.pos[0], -1, 1, .01],
                    onChange: v => {vals[state].lightc.pos[0] = v;},
                    _ref: { set: 'lightc', sub: 'pos', idx: 0 }
                },
                {
                    y: [pgm.uniforms.lightc.pos[1], -1, 1, .01],
                    onChange: v => {vals[state].lightc.pos[1] = v;},
                    _ref: { set: 'lightc', sub: 'pos', idx: 1 }
                },
                {
                    z: [pgm.uniforms.lightc.pos[2], -2, 2, .01],
                    onChange: v => {vals[state].lightc.pos[2] = v;},
                     _ref: { set: 'lightc', sub: 'pos', idx: 2 }
                },
                {
                    pow: [pgm.uniforms.lightc.pow, 1, 40, .01],
                    onChange: (v)=>{vals[state].lightc.pow = v;},
                     _ref: { set: 'lightc', sub: 'pow' }
                },
                {
                    spec: [pgm.uniforms.lightc.spec, 0, 1, .01],
                    onChange: (v)=>{vals[state].lightc.spec = v;},
                     _ref: { set: 'lightc', sub: 'spec' }
                },
                {
                    diff: [pgm.uniforms.lightc.diff, 0, 1, .01],
                    onChange: (v)=>{vals[state].lightc.diff = v;},
                     _ref: { set: 'lightc', sub: 'diff'}
                },
                {
                    dz: [-pgm.uniforms.lightc.dz, 0, 2, .001],
                    onChange: v =>{
                        vals[state].lightc.dz = -v;
                    },
                     _ref: { set: 'lightc', sub: 'dz', inv: true}
                },
                {
                    am: [pgm.uniforms.lightc.am, 0, 1, .001],
                    onChange: v =>{vals[state].lightc.am = v;},
                     _ref: { set: 'lightc', sub: 'am'}
                },
                {
                    lev: [pgm.uniforms.lightc.lev, 0, 1.5, .01],
                    onChange: (v)=>{vals[state].lightc.lev = v;},
                    _ref: { set: 'lightc', sub: 'lev' }
                },
                {
                    hue: [0, 0, 1, .001],
                    onChange: (v)=>{
                        valsg[state].c_hsv[0] = v;
                        vals[state].lightc.col = hsv2rgb(...valsg[state].c_hsv);
                    },
                    _ref: {cb: (ref)=>{ ref.setValue(valsg[state].c_hsv[0])}}
                },
            ]
        }
    ]
 
};

export default pgm;

function setVals(v, u, f){
    for(let k in v[0]){
        if(v[0][k].constructor === Object){
            for(let _k in v[0][k]){
                if(v[0][k][_k] instanceof Array){
                    for(let i = 0; i < v[0][k][_k].length; i++)
                        u[k][_k][i] = lerp(v[0][k][_k][i], v[1][k][_k][i], v[2][k][_k][i], f);
                }else u[k][_k] = lerp(v[0][k][_k], v[1][k][_k], v[2][k][_k], f);
            }
        }
        else{
            if(v[0][k] instanceof Array){
                for(let i = 0; i < v[0][k].length; i++)
                    u[k][i] = lerp(v[0][k][i], v[1][k][i], v[2][k][i], f);
            }
            else u[k] = lerp(v[0][k], v[1][k], v[2][k], f);
        }
    }
}

function updateGui(fields, vals){
    for(let o of fields){
        if(o.fields)
            updateGui(o.fields, vals);
        else if(o.ref.set && vals[o.ref.set] != null){
            let val = o.ref.sub ? vals[o.ref.set][o.ref.sub] : vals[o.ref.set];
            if(o.ref.inv) val *= -1;
            if(o.ref.idx == null) o.ref.setValue(val);
            else o.ref.setValue(val[o.ref.idx]);
        }
        else if(o.ref.cb) o.ref.cb(o.ref);
    }
}

function loadPreset(str){
    try{
        let o = JSON.parse(str);
        if(!o) return;
        vals[0] = o.vals[0];
        vals[1] = o.vals[1];
        vals[2] = o.vals[2];
        valsg[0] = o.valsg[0];
        valsg[1] = o.valsg[1];
        valsg[2] = o.valsg[2];
        m.idx = valsg[0].idx;
        loadModel(pgm, m);  
        pgm.gui.fields[0].fields[0].ref.setValue(state);    
    }catch(e){}
}

function copyMem(m1, m2, from, to){
    m1[to] = JSON.parse(JSON.stringify(m1[from]));
    m2[to] = JSON.parse(JSON.stringify(m2[from]));
}

function setupMem(targetArray, source, num){
    for(let i = 0; i < num; i++){
        let c = JSON.parse(JSON.stringify(source));
        const {rmat, vmat, pmat, ...o} = c;
        targetArray.push(o);
    }
}

function hsv2rgb(h,s,v){ //[ 0,1]->[0,1]
  let f= (n,k=(n+h*6)%6) => v - v*s*Math.max( Math.min(k,4-k,1), 0);     
  return [f(5),f(3),f(1)];       
}

function ntagonal(o, n){
    const vmap = {};
    const v = o.vertices.v;
    for(let f of o.elements.f.v){
        for(let _v of f){
            let vk = v[_v].join('');
            vmap[vk] ??= {i: _v ,a: 0};
            vmap[vk].a++;
        }
    }
    // console.log(Object.values(vmap).map(e=>e.a));
    return Object.values(vmap).filter(e=>e.a==n).map(e=>e.i);
} 

function concavePoly(model, n, lev=.5){
    let arr = (n instanceof Array)? n : ntagonal(model, n) ;
    let cmodel = {
        ...model,
        vertices: {
            v: model.vertices.v.map(a=>a),
            vn: model.vertices.vn.map(a=>a)
        }
    };
    for(let i of arr) cmodel.vertices.v[i] = mults(cmodel.vertices.v[i], lev);
    let norm = getNormals(cmodel, true);
    cmodel.vertices.vn = norm.vn;
    return cmodel;
}

function mults(v, s){
    return [v[0]*s, v[1]*s, v[2]*s];
}