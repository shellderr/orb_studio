import {solids, polyhedra, models} from './model.js';
import {loadObj, getNormals} from './loader.js';
import * as mat4 from '../lib/glmat/mat4.js';
import * as THREE from '../lib/three-bvh.min.js';
const {ADDITION, SUBTRACTION, INTERSECTION, DIFFERENCE, Brush, Evaluator} = THREE;
var p_fov = 1.222, p_near = .08, p_far = 20;
const eye = {x: 0, y: 0, z: -1.5, tx: 0, ty: 0, tz: 1};
const m = {
    geom: null,
    model: null,
    cmodel: null,
    r1: null,
    q1: null,
};
const vs =/*glsl*/`#version 300 es
    precision mediump float;
    in vec4 position;
    in vec4 normal;
    uniform mat4 pmat;
    uniform mat4 vmat;
    uniform mat4 rmat;
    uniform float zdist;
    out vec3 vnormal;
    out vec3 vdir;
    out vec3 vpos;

    struct Light {
        vec3 pos;
        vec3 dir;
        vec3 col;
        float pow;
        float spec;
        float diff;
        float dz;
        float am;
        float lev;
    };

    uniform Light lighta;
    out Light alight;

    void main() {
        vec4 pos = position*rmat;
        vpos = pos.xyz;
        vnormal = (normal*rmat).xyz;
        pos.z += zdist; 
        vdir = vec3(0,0,-1) - pos.xyz;
        gl_Position = pmat*vmat*pos;
        alight = lighta;
        alight.dir = alight.pos*vec3(-1,-1,1) - pos.xyz;
    }
`;

const fs = /*glsl*/`#version 300 es
    precision mediump float;
    in vec3 vnormal;
    in vec3 vdir;
    in vec3 vpos;
    in float uvz;
    out vec4 fragColor;
    uniform float smoothn;

    struct Light {
        vec3 pos;
        vec3 dir;
        vec3 col;
        float pow;
        float spec;
        float diff;
        float dz;
        float am;
        float lev;
    };

    in Light alight;

    float lighting(vec3 vnorm, vec3 lpos, float p, float s, float d, float dz, float am){
        vec3 light = normalize(lpos);
        vec3 ray = reflect(-light, vnorm);
        float spec = pow(max(0., dot(light, ray)) ,p);
        float diff = max(0., dot(vec3(light.xy,light.z+dz), vnorm));
        return .3*am+d*diff+s*spec;
    }

    float fresnel(vec3 vnorm, vec3 lpos){
        return 0.;
    }

    void main(){
        vec3 n = normalize(cross(dFdx(vpos), dFdy(vpos)));
        vec3 norm = mix(n, normalize(vnormal), smoothn);
        float l = lighting(norm, alight.dir, alight.pow, alight.spec, alight.diff, alight.dz, alight.am);
        vec3 c = l*alight.col*alight.lev;
        fragColor = vec4(c, 1); 
    }

`;

function loadModel(pgm, g){
    g.geom = new THREE.BufferGeometry();
    let model = loadObj(Object.values(polyhedra)[3], .8);
    let cmodel = concavePoly(model, 10, .6);
    let m = g.geom.arrays = {a: {v: [], n: []},b: {v: [], n: []}};
    for(let t of model.indices.v)
        for(let i of t) m.a.v.push(...model.vertices.v[i]);
    for(let t of model.indices.v)
        for(let i of t) m.a.n.push(...model.vertices.vn[i]);
    for(let t of cmodel.indices.v)
        for(let i of t) m.b.v.push(...cmodel.vertices.v[i]);
    for(let t of cmodel.indices.v)
        for(let i of t) m.b.n.push(...cmodel.vertices.vn[i]);
    m.a.v = new Float32Array(m.a.v);
    m.a.n = new Float32Array(m.a.n);
    m.b.v = new Float32Array(m.b.v);
    m.b.n = new Float32Array(m.b.n);
    g.geom.setAttribute('position', new THREE.BufferAttribute(new Float32Array(m.a.v), 3)); 
    g.geom.setAttribute('normal', new THREE.BufferAttribute(new Float32Array(m.a.n), 3)); 
    pgm.arrays.position.data = g.geom.attributes.position.array;
    pgm.arrays.normal.data = g.geom.attributes.normal.array;
    pgm.count = g.geom.attributes.position.count;
    console.log(g.geom.attributes);
}

function lerp(a, b, t){
    return a + t * (b - a);
}

function morph(g, f=0){
    let av = g.arrays.a.v;
    let bv = g.arrays.b.v;
    let pv = g.attributes.position.array;
    // let an = g.arrays.a.n;
    // let bn = g.arrays.b.n;
    // let pn = g.attributes.normal.array;
    for(let i = 0; i < pv.length; i += 3){
        pv[i]   = lerp(av[i], bv[i], f);
        pv[i+1] = lerp(av[i+1], bv[i+1], f);
        pv[i+2] = lerp(av[i+2], bv[i+2], f);
        // pn[i]   = lerp(an[i], bn[i], f);
        // pn[i+1] = lerp(an[i+1], bn[i+1], f);
        // pn[i+2] = lerp(an[i+2], bn[i+2], f);
    }
}

function updateBuffers(pgm, geometry){
    let g = geometry.attributes; 
    pgm.gl.bindBuffer(pgm.gl.ARRAY_BUFFER, pgm.arrays.position.buffer);
    pgm.gl.bufferSubData(pgm.gl.ARRAY_BUFFER, 0, g.position.array);
    pgm.gl.bindBuffer(pgm.gl.ARRAY_BUFFER, pgm.arrays.normal.buffer);
    pgm.gl.bufferSubData(pgm.gl.ARRAY_BUFFER, 0, g.normal.array);
    pgm.count = g.position.count;  
}

function setup(pgm){
    mat4.perspective(pgm.uniforms.pmat, p_fov, pgm.res[0]/pgm.res[1], p_near, p_far);
    mat4.lookAt(pgm.uniforms.vmat,[eye.x,eye.y,eye.z],[0,eye.tx,eye.tz], [0,1,0]);
    loadModel(pgm, m);
}

function render(pgm){
    let t = pgm.uniforms.time*.5;
    let f = Math.abs((t-Math.floor(t))*2-1)*2
    morph(m.geom, f)
    updateBuffers(pgm, m.geom);
    let x = pgm.uniforms.mouse[0]-.5;
    let y = pgm.uniforms.mouse[1]-.5;
    mat4.rotate(pgm.uniforms.rmat, pgm.uniforms.rmat, Math.min(.07*Math.hypot(y, x),.05), [-y,-x,0]);
}

const pgm = {
    arrays: {
        position: {
            components: 3,
            data: []
        },
        normal: {
            components: 3,
            data: []
        }
    },
    time: 0,
    rtime: 0,
    uniforms: {
        pmat: [1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1],
        vmat: [1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1],
        rmat: [1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1],
        zdist: .5,
        smoothn: 0,
        lighta: {
            pos: [.0, .0, -.8],
            dir: [0,0,-1],
            col: [1,0,0],
            pow: 30,
            spec: 1,
            diff: .7,
            dz: 0,
            am: 0,
            lev: .6
        }
    },
    a_hsv: [0,1,1],
    vs: vs,
    fs: fs,
    clearcolor: [0,0,0,1],
    drawMode: 'TRIANGLES',
    setupcb: setup,
    rendercb: render

};

export default pgm;

pgm.gui = {
    name: null,
    open: true,
    fields:[
        {
            z: [pgm.uniforms.zdist, -2, 10, .01],
            onChange: v => {pgm.uniforms.zdist = v;}
        },
        {
            fov: [p_fov, Math.PI/4, Math.PI, .01],
            onChange: v => {
                mat4.perspective(pgm.uniforms.pmat, (p_fov = v), pgm.res[0]/pgm.res[1], p_near, p_far);
            }
        },
        {
            name: 'view',
            open: true,
            updateFrame: true,
            fields: [
                {   
                    eyex: [eye.x, -2, 2, .001],
                    onChange: v =>{
                        mat4.lookAt(pgm.uniforms.vmat,[(eye.x=v),eye.y,eye.z],[eye.tx,eye.ty,eye.tz], [0,1,0]);
                    }
                },
                {   
                    eyey: [eye.y, -2, 2, .001],
                    onChange: v =>{
                        mat4.lookAt(pgm.uniforms.vmat,[eye.x,(eye.y=v),eye.z],[eye.tx,eye.ty,eye.tz], [0,1,0]);
                    }
                },
                {
                    eyez: [eye.z, -10, 5, .001],
                    onChange: v =>{
                        mat4.lookAt(pgm.uniforms.vmat,[eye.x,eye.y,(eye.z=v)],[eye.tx,eye.ty,eye.tz], [0,1,0]);
                    }
                },
                {
                    targetx: [eye.tx, -2, 2, .001],
                    onChange: v => {
                         mat4.lookAt(pgm.uniforms.vmat,[eye.x,eye.y,eye.z],[(eye.tx=v),eye.ty,eye.tz], [0,1,0]);
                    }
                },
                {
                    targety: [eye.ty, -2, 2, .001],
                    onChange: v => {
                         mat4.lookAt(pgm.uniforms.vmat,[eye.x,eye.y,eye.z],[eye.tx,(eye.ty=v),eye.tz], [0,1,0]);
                    }
                },
            ]
        },
        {
            name: 'light',
            open: true,
            updateFrame: true,
            fields: [
                {
                    x: [pgm.uniforms.lighta.pos[0], -1, 1, .01],
                    onChange: v => {pgm.uniforms.lighta.pos[0] = v;}
                },
                {
                    y: [pgm.uniforms.lighta.pos[1], -1, 1, .01],
                    onChange: v => {pgm.uniforms.lighta.pos[1] = v;}
                },
                {
                    z: [pgm.uniforms.lighta.pos[2], -2, 2, .01],
                    onChange: v => {pgm.uniforms.lighta.pos[2] = v;}
                },
                {
                    pow: [pgm.uniforms.lighta.pow, 1, 40, .01],
                    onChange: (v)=>{pgm.uniforms.lighta.pow = v;}
                },
                {
                    spec: [pgm.uniforms.lighta.spec, 0, 1, .01],
                    onChange: (v)=>{pgm.uniforms.lighta.spec = v;}
                },
                {
                    diff: [pgm.uniforms.lighta.diff, 0, 1, .01],
                    onChange: (v)=>{pgm.uniforms.lighta.diff = v;}
                },
                {
                    dz: [-pgm.uniforms.lighta.dz, 0, 2, .001],
                    onChange: v =>{pgm.uniforms.lighta.dz = -v;}
                },
                {
                    am: [pgm.uniforms.lighta.am, 0, 1, .001],
                    onChange: v =>{pgm.uniforms.lighta.am = v;}
                },
                {
                    lev: [pgm.uniforms.lighta.lev, 0, 1.5, .01],
                    onChange: (v)=>{pgm.uniforms.lighta.lev = v;}
                },
                {
                    hue: [0, 0, 1, .001],
                    onChange: (v)=>{
                        pgm.a_hsv[0] = v;
                        pgm.uniforms.lighta.col = hsv2rgb(...pgm.a_hsv);
                    },
                },
            ]
        }
    ]
};

function hsv2rgb(h,s,v){ //[ 0,1]->[0,1]
  let f= (n,k=(n+h*6)%6) => v - v*s*Math.max( Math.min(k,4-k,1), 0);     
  return [f(5),f(3),f(1)];       
}

function ntagonal(o, n){
    const vmap = {};
    const v = o.vertices.v;
    for(let f of o.elements.f.v){
        for(let _v of f){
            let vk = v[_v].join('');
            vmap[vk] ??= {i: _v ,a: 0};
            vmap[vk].a++;
        }
    }
    // console.log(Object.values(vmap).map(e=>e.a));
    return Object.values(vmap).filter(e=>e.a==n).map(e=>e.i);
} 

function concavePoly(model, n, lev=.5){
    let arr = (n instanceof Array)? n : ntagonal(model, n) ;
    let cmodel = {
        ...model,
        vertices: {
            v: model.vertices.v.map(a=>a),
            vn: model.vertices.vn.map(a=>a)
        }
    };
    for(let i of arr) cmodel.vertices.v[i] = mults(cmodel.vertices.v[i], lev);
    let norm = getNormals(cmodel, true);
    cmodel.vertices.vn = norm.vn;
    return cmodel;
}

function mults(v, s){
    return [v[0]*s, v[1]*s, v[2]*s];
}