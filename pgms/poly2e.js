import polyhedra from './polyhedra.js';
import poly2 from './polyhedra3.js';
import {loadObj, getNormals} from './loader.js';
import * as mat4 from '../lib/glmat/mat4.js';
import * as THREE from '../lib/three-bvh.min.js';
const {ADDITION, SUBTRACTION, INTERSECTION, DIFFERENCE, Brush, Evaluator} = THREE;
var p_fov = 1.222, p_near = .08, p_far = 20, m0 = 1, m1 = 1, r_ = 1;
const eye = {x: 0, y: 0, z: -1.5, tx: 0, ty: 0, tz: 1};
const m = {
    idx: 0,
    geom: null,
    r1: null,
    q1: null,
};
const vs =/*glsl*/`#version 300 es
    precision mediump float;
    in vec4 position;
    in vec4 normal;
    uniform mat4 pmat;
    uniform mat4 vmat;
    uniform mat4 rmat;
    uniform float zdist;
    out vec3 vnormal;
    out vec3 vdir;
    out vec3 vpos;

    struct Light {
        vec3 pos;
        vec3 dir;
        vec3 col;
        float pow;
        float spec;
        float spec0;
        float diff;
        float dz;
        float am;
        float lev;
    };

    uniform Light lighta;
    out Light alight;

    void main() {
        vec4 pos = position*rmat;
        vpos = pos.xyz;
        vnormal = (normal*rmat).xyz;
        pos.z += zdist; 
        vdir = vec3(0,0,-1) - pos.xyz;
        gl_Position = pmat*vmat*pos;
        alight = lighta;
        alight.dir = alight.pos*vec3(-1,-1,1) - pos.xyz;
    }
`;

const fs = /*glsl*/`#version 300 es
    precision mediump float;
    in vec3 vnormal;
    in vec3 vdir;
    in vec3 vpos;
    in float uvz;
    out vec4 fragColor;
    uniform float smoothn;
    uniform float nz;
    uniform float fres;

    struct Light {
        vec3 pos;
        vec3 dir;
        vec3 col;
        float pow;
        float spec;
        float spec0;
        float diff;
        float dz;
        float am;
        float lev;
    };

    in Light alight;


    float lighting(vec3 vnorm, vec3 lpos, float p, float s, float s0, float d, float dz, float am){
        vec3 light = normalize(lpos);
        vec3 ray = reflect(-light, vnorm);
        float spec = pow(max(0., dot(light, ray)) ,p);
        float spec0 = max(0., dot(light, ray));
        float diff = max(0., dot(vec3(light.xy,light.z+dz), vnorm));
        return .3*am + d*diff + s*spec + s0 *spec0;
    }

    float fresnel(vec3 vnorm, vec3 lpos, float dz){
        vec3 light = normalize(lpos);
        return pow(1.-max(dot(vec3(light.xy,light.z+dz), vnorm), 0.), 4.);
    }

    void main(){
        vec3 n = normalize(cross(dFdx(vpos), dFdy(vpos)));
        n.z -= nz;
        vec3 norm = mix(normalize(n), normalize(vnormal), smoothn);
        float l = lighting(norm, alight.dir, alight.pow, alight.spec, alight.spec0, alight.diff, alight.dz, alight.am);
        vec3 c = l*alight.col*alight.lev + fres*vec3(1.,.3,.8)*fresnel(norm, alight.dir, alight.dz);
        fragColor = vec4(c, 1); 
    }

`;

function loadModel(pgm, g, init=0){
    g.geom = new THREE.BufferGeometry();
    let model = loadObj(Object.values(poly2)[g.idx], .8);
    let m = g.geom.arrays = {v: [], n: []};
    m.i = ngons(model).map(e=>ngonv(model,e));
    for(let t of model.indices.v) for(let i of t) m.v.push(...model.vertices.v[i]);
    for(let t of model.indices.vn) for(let i of t) m.n.push(...model.vertices.vn[i]);  
    m.v = new Float32Array(m.v);
    m.n = new Float32Array(m.n);
    g.geom.setAttribute('position', new THREE.BufferAttribute(new Float32Array(m.v), 3)); 
    g.geom.setAttribute('normal', new THREE.BufferAttribute(new Float32Array(m.n), 3)); 
    pgm.arrays.position.data = g.geom.attributes.position.array;
    pgm.arrays.normal.data = g.geom.attributes.normal.array;
    if(!init){pgm.gl.bindVertexArray(null); pgm.mgl.setBuffers(pgm.gl, pgm);}
}

function ngons(o){
    const vmap = {};
    const v = o.indices.v;
    for(let f of o.elements.f.v){
        for(let _v of f){
            let vk = v[_v].join('');
            vmap[vk] ??= {i: _v ,a: 0};
            vmap[vk].a++;
        }
    }
    let av = Object.values(vmap).map(e=>e.a); 
    av = [...new Set(av)].sort((a,b)=>{return a<b});
    return Array.from(av, (i)=>{
        return Object.values(vmap).filter(e=>e.a==i).map(e=>e.i);
    });
} 

function ngonv(model, arr){
    let k = 0, a = [];
    for(let t of model.indices.v) for(let i of t){
        if(arr.includes(i)) a.push(k, k+1, k+2); 
        k += 3;
    } return a;
}

function morph(g, f0=1, f1=1){
    let av = g.arrays.v;
    let pv = g.attributes.position.array;
    let v0 = g.arrays.i[0];
    let v1 = g.arrays.i[1];
    for(let i of v0) pv[i] = av[i]*f0;
    for(let i of v1) pv[i] = av[i]*f1;
}

function updateBuffers(pgm, geometry){
    let g = geometry.attributes; 
    pgm.gl.bindBuffer(pgm.gl.ARRAY_BUFFER, pgm.arrays.position.buffer);
    pgm.gl.bufferSubData(pgm.gl.ARRAY_BUFFER, 0, g.position.array);
    pgm.count = g.position.count;  
}

function setup(pgm){
    mat4.perspective(pgm.uniforms.pmat, p_fov, pgm.res[0]/pgm.res[1], p_near, p_far);
    mat4.lookAt(pgm.uniforms.vmat,[eye.x,eye.y,eye.z],[0,eye.tx,eye.tz], [0,1,0]);
    loadModel(pgm, m, 1);
}

function render(pgm){
    if(m.geom.arrays.i.length > 1) morph(m.geom, m0, m1);
    updateBuffers(pgm, m.geom);
    let x = pgm.uniforms.mouse[0]-.5;
    let y = pgm.uniforms.mouse[1]-.5;
    mat4.rotate(pgm.uniforms.rmat, pgm.uniforms.rmat, r_*Math.min(.07*Math.hypot(y, x),.05), [-y,-x,0]);
}

function hsv2rgb(h,s,v){
    let f= (n,k=(n+h*6)%6) => v - v*s*Math.max( Math.min(k,4-k,1), 0);     
    return [f(5),f(3),f(1)];       
}

const pgm = {
    arrays: {
        position: {
            components: 3,
            data: []
        },
        normal: {
            components: 3,
            data: []
        }
    },
    time: 0,
    rtime: 0,
    uniforms: {
        pmat: [1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1],
        vmat: [1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1],
        rmat: [1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1],
        zdist: .5,
        smoothn: 0,
        fres: 0,
        nz: 0,
        lighta: {
            pos: [.0, .0, -.8],
            dir: [0,0,-1],
            col: [0,.5,1],
            pow: 30,
            spec: 1,
            spec0: 0,
            diff: .7,
            dz: 0,
            am: 0,
            lev: 1
        }
    },
    a_hsv: [0,1,1],
    vs: vs,
    fs: fs,
    clearcolor: [0,0,0,1],
    drawMode: 'TRIANGLES',
    setupcb: setup,
    rendercb: render

};

export default pgm;

pgm.gui = {
    name: null,
    open: true,
    fields:[
        {
            idx: [m.idx, 0,Object.keys(poly2).length-1, 1],
            onChange: (v)=>{ m.idx = v; loadModel(pgm,m);}
        },
        {
            z: [pgm.uniforms.zdist, -2, 10, .01],
            onChange: v => {pgm.uniforms.zdist = v;}
        },
        {
            normz: [pgm.uniforms.nz, -2, 2, .01],
            onChange: v => {pgm.uniforms.nz = v;}
        },
        {
            fres: [pgm.uniforms.fres, 0, 1, .001],
            onChange: v => {pgm.uniforms.fres = v;}
        },
        {
            fov: [p_fov, Math.PI/4, Math.PI, .01],
            onChange: v => {
                mat4.perspective(pgm.uniforms.pmat, (p_fov = v), pgm.res[0]/pgm.res[1], p_near, p_far);
            }
        },
        {
            rot: [r_, 0, 1, .001],
            onChange:(v)=>{r_=v}
        },
        {
            m1: [m0, .5, 1.5, .001],
            onChange:(v)=>{m0=v}
        },
        {
            m2: [m1, .5, 1.5, .001],
            onChange:(v)=>{m1=v}
        },
        {
            name: 'view',
            open: true,
            updateFrame: true,
            fields: [
                {   
                    eyex: [eye.x, -2, 2, .001],
                    onChange: v =>{
                        mat4.lookAt(pgm.uniforms.vmat,[(eye.x=v),eye.y,eye.z],[eye.tx,eye.ty,eye.tz], [0,1,0]);
                    }
                },
                {   
                    eyey: [eye.y, -2, 2, .001],
                    onChange: v =>{
                        mat4.lookAt(pgm.uniforms.vmat,[eye.x,(eye.y=v),eye.z],[eye.tx,eye.ty,eye.tz], [0,1,0]);
                    }
                },
                {
                    eyez: [eye.z, -10, 5, .001],
                    onChange: v =>{
                        mat4.lookAt(pgm.uniforms.vmat,[eye.x,eye.y,(eye.z=v)],[eye.tx,eye.ty,eye.tz], [0,1,0]);
                    }
                },
                {
                    targetx: [eye.tx, -2, 2, .001],
                    onChange: v => {
                         mat4.lookAt(pgm.uniforms.vmat,[eye.x,eye.y,eye.z],[(eye.tx=v),eye.ty,eye.tz], [0,1,0]);
                    }
                },
                {
                    targety: [eye.ty, -2, 2, .001],
                    onChange: v => {
                         mat4.lookAt(pgm.uniforms.vmat,[eye.x,eye.y,eye.z],[eye.tx,(eye.ty=v),eye.tz], [0,1,0]);
                    }
                },
            ]
        },
        {
            name: 'light',
            open: true,
            updateFrame: true,
            fields: [
                {
                    x: [pgm.uniforms.lighta.pos[0], -1, 1, .01],
                    onChange: v => {pgm.uniforms.lighta.pos[0] = v;}
                },
                {
                    y: [pgm.uniforms.lighta.pos[1], -1, 1, .01],
                    onChange: v => {pgm.uniforms.lighta.pos[1] = v;}
                },
                {
                    z: [pgm.uniforms.lighta.pos[2], -2, 2, .01],
                    onChange: v => {pgm.uniforms.lighta.pos[2] = v;}
                },
                {
                    pow: [pgm.uniforms.lighta.pow, 1, 40, .01],
                    onChange: (v)=>{pgm.uniforms.lighta.pow = v;}
                },
                {
                    spec: [pgm.uniforms.lighta.spec, 0, 1, .01],
                    onChange: (v)=>{pgm.uniforms.lighta.spec = v;}
                },
                {
                    sdiff: [pgm.uniforms.lighta.spec0, 0, 1, .01],
                    onChange: (v)=>{pgm.uniforms.lighta.spec0 = v;}
                },
                {
                    diff: [pgm.uniforms.lighta.diff, 0, 1, .01],
                    onChange: (v)=>{pgm.uniforms.lighta.diff = v;}
                },
                {
                    dz: [-pgm.uniforms.lighta.dz, 0, 2, .001],
                    onChange: v =>{pgm.uniforms.lighta.dz = -v;}
                },
                {
                    am: [pgm.uniforms.lighta.am, 0, 1, .001],
                    onChange: v =>{pgm.uniforms.lighta.am = v;}
                },
                {
                    lev: [pgm.uniforms.lighta.lev, 0, 1.5, .01],
                    onChange: (v)=>{pgm.uniforms.lighta.lev = v;}
                },
                {
                    hue: [pgm.a_hsv[0], 0, 1, .001],
                    onChange: (v)=>{
                        pgm.a_hsv[0] = v;
                        pgm.uniforms.lighta.col = hsv2rgb(...pgm.a_hsv);
                    },
                },
            ]
        }
    ]
};