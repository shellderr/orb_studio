import resolve from '@rollup/plugin-node-resolve';
import terser from '@rollup/plugin-terser';
export default {
    input: 'index.js', // export * from 'three'; export * from 'three-bvh-csg';
    output: [
        {
            file: 'three-bvh.min.js',
            format: 'es',
            plugins: [terser()]        
        }
    ],
    plugins: [resolve()]
};